﻿using Infrastructure.IServices.IMasterService;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Models.Master.DTO;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProductEntryController : ControllerBase
    {
        private readonly IProductEntryService _productEntryService;
        private readonly ILogger<ProductEntryController> _logger;

        public ProductEntryController(IProductEntryService productEntryService, ILogger<ProductEntryController> logger)
        {
            _productEntryService = productEntryService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<bool>> EnterProduct(ProductMasterEntryDto masterEntryDto)
        {
            try
            {
                _logger.LogInformation($"{masterEntryDto}");
                return await _productEntryService.CreateProduct(masterEntryDto);
            }
            catch (Exception e)
            {
                _logger.LogError($"Product :: {e.Message}");
                throw;
            }
        }

        [HttpGet]
        public async Task<ActionResult<List<ProductMasterEntryDto>>> ProductList()
        {
            try
            {
                _logger.LogInformation("");
               return await  _productEntryService.ProductList();
            }
            catch (Exception e)
            {
                _logger.LogError($"{e.Message}");
                throw;
            }
        }

        [HttpGet("id")]
        public async Task<ActionResult<List<ProductDetailEntryDto>>> ProductsById(int id)
        {
            try
            {
                _logger.LogInformation("");
                return await _productEntryService.ProductsById(id);
            }
            catch (Exception e)
            {
                _logger.LogError($"{e.Message}");
                throw;
            }
        }
    }
}

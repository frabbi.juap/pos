﻿using Infrastructure.IServices.IHelperService;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Models.Master.DTO;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SearchProductController : ControllerBase
    {
        private readonly IProductSearchService _productSearchService;
        private readonly ILogger<SearchProductController> _logger;

        public SearchProductController(IProductSearchService productSearchService, ILogger<SearchProductController> logger)
        {
            this._productSearchService = productSearchService;
            this._logger = logger;
        }

        [HttpGet("name")]
        public async Task<ActionResult<List<ProductDetailEntryDto>>> SearchProduct(string name)
        {
            var products = await _productSearchService.SearchProducts(name);

            return products;
        }
    }
}

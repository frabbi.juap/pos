﻿using Infrastructure.IServices.ISettingService;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using Models.Settings.DTO;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Controllers.Settings
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SupplierController : ControllerBase
    {
        private readonly ISupplierService _supplierService; 

        public SupplierController(ISupplierService supplierService )
        {
            _supplierService = supplierService;             
        }

        [HttpGet]
        public async Task<ActionResult<List<SupplierDto>>> SupplierList ()
        {
            return await _supplierService.SupplierList();
        }

        [HttpPost]
        public async Task<ActionResult<bool>> CreateCategory(SupplierDto supplier)
        {
            return await _supplierService.CreateSupplier(supplier);
        }
    }
}

﻿using Infrastructure.IServices.IHelperService;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers.Settings
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class VoucherGenerationController : ControllerBase
    {
        private readonly IGenerateVoucherService _generateVoucher;
        private readonly ILogger<VoucherGenerationController> _logger;

        public VoucherGenerationController(IGenerateVoucherService generateVoucher, ILogger<VoucherGenerationController> logger)
        {
            _generateVoucher = generateVoucher;
           _logger = logger;
        }
        [HttpGet]
        public async Task<ActionResult<string>> GenerateVoucher()
        {
            try
            {
                return await _generateVoucher.GenerateVoucher();
            }
            catch (Exception e)
            {
                _logger.LogError($"Generate Voucher :: {e.Message}");
                throw;
            }
        }
    }
}

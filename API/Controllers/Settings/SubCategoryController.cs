﻿using Infrastructure.Common;
using Infrastructure.IServices.ISettingService;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Models.Settings.DTO;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Controllers.Settings
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SubCategoryController : ControllerBase
    {
        private readonly ISubCategoryService _subCategoryService;
        private readonly ILogger<SubCategoryController> _logger;

        public SubCategoryController(ISubCategoryService subCategoryService, ILogger<SubCategoryController> logger)
        {
            _subCategoryService = subCategoryService;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<List<SubCategoryDto>>> SubCategoryList()
        {
            try
            {
                _logger.LogInformation("Sub category List");
                return await _subCategoryService.SubCategoryList();
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }

        [HttpPost]
        public async Task<ActionResult<bool>> CreateSubCategory(SubCategoryDto subCategoryDto)
        {
            try
            {
                _logger.LogInformation($"Sub-Category : {subCategoryDto.SubCategoryName}, {subCategoryDto.CategoryId} ");
                var result = await _subCategoryService.CreateSubCategory(subCategoryDto);
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError("Error : ", e.Message);
                throw new RestException(System.Net.HttpStatusCode.BadRequest);
            }
        }
    }
}

﻿using Infrastructure.Common;
using Infrastructure.IServices.ISettingService;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Models.Settings;
using Models.Settings.DTO;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Controllers.Settings
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BrandController : ControllerBase
    {
        private readonly IBrandService _brandService;
        private readonly ILogger<BrandController> _logger;

        public BrandController(IBrandService brandService, ILogger<BrandController> logger )
        {
            _brandService = brandService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<bool>> CreateBrand(BrandDto brand)
        {
            try
            {
                _logger.LogInformation($"{brand.Name}");
                return await _brandService.CreateBrand(brand);
            }
            catch (Exception e)
            {
                _logger.LogError($"Error : {e.Message} ");
                throw new RestException(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }

        [HttpGet]
        public async Task<ActionResult<List<BrandDto>>> BrandList()
        {
            try
            {
                _logger.LogInformation("Brand List");
                return await _brandService.BrandList();
            }
            catch (Exception e)
            {
                _logger.LogError($"Error on brand  : {e.Message}");
                throw new RestException(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}

﻿using Infrastructure.IServices.ISettingService;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using Models.Settings;
using Models.Settings.DTO;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Controllers.Settings
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpGet]
        public async Task<ActionResult<List<CategoryDto>>> CategoryList()
        {
            return await _categoryService.CategoryList();
        }

        [HttpPost]
        public async Task<ActionResult<bool>> CreateCategory(CategoryDto category)
        {
            return await _categoryService.CreateCategory(category);
        }
    }
}

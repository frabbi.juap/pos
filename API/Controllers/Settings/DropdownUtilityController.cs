﻿using Infrastructure.IServices.ISettingService;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Models.Settings.DTO;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers.Settings
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DropdownUtilityController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        private readonly IBrandService _brandService;
        private readonly IDropdownUtilityService _dropdownService;

        public DropdownUtilityController(ICategoryService categoryService, IBrandService brandService, IDropdownUtilityService dropdownService)
        {
            _categoryService = categoryService;
            _brandService = brandService;
            _dropdownService = dropdownService;
        }

        [HttpGet()]
        public async Task<ActionResult<List<BrandDto>>> BrandList()
        {
            return await _brandService.BrandList();
        }

        [HttpGet("CategoryDropdownListByBrandId/{id}")]
        public async Task<ActionResult<List<CategoryDto>>> CategoryDropdownListByBrandId(int id)
        {
            return await _dropdownService.CategoryListByBrandId(id);
        }

        [HttpGet("SubCategoryDropdownListByCategoryId/{id}")]
        public async Task<ActionResult<List<SubCategoryDto>>> SubCategoryDropdownListByCategoryId(int id)
        {
            return await _dropdownService.SubCategoryListByCategoryId(id);
        }

    }
}

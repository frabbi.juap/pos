﻿using Infrastructure.Common;
using Infrastructure.IServices.IMasterService;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Models.Master.DTO;

using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace API
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProductSaleController : ControllerBase
    {
        private readonly IProductSaleService _saleService;
        private readonly ILogger<ProductSaleController> _logger;

        public ProductSaleController(IProductSaleService saleService, ILogger<ProductSaleController> logger)
        {
            _saleService = saleService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<bool>> SellProduct(ProductSaleMasterDto productSale)
        {
            try
            {
                return await _saleService.SellProduct(productSale);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        [HttpGet]
        public async Task<ActionResult<List<ProductSaleMasterDto>>> SoldProducts()
        {
            try
            {
                return await _saleService.SoldProductList();
            }
            catch (Exception e)
            {

                throw new RestException(HttpStatusCode.NotFound,"data not found");
            }
        }

        [HttpGet("id")]
        public async Task<ActionResult<List<ProductSaleDetailDto>>> SoldProductDetail(int id)
        {
            try
            {
                return await _saleService.SoldProductsById(id);
            }
            catch (Exception e)
            {

                throw new RestException(HttpStatusCode.NotFound, e.Message);
            }
        }

        [HttpDelete("id")]
        public async Task<ActionResult< bool>> DeleteSoldProduct(int id)
        {
            try
            {
                return await _saleService.DeleteSoldProduct(id);
            }
            catch (Exception e)
            {

                throw new RestException(HttpStatusCode.NotFound, "data not found");
            }
        }
    }
}

﻿using Newtonsoft.Json;

using POSDesktop.Models;
using POSDesktop.Views;

using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;

namespace POSDesktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ApiCallModel ApiCall;
        string token;
        public MainWindow()
        {
            ApiCall = new ApiCallModel(token);
            InitializeComponent();
        }



        
        private void button_login_Click(object sender, RoutedEventArgs e)
        {
            Login login = new Login();
            login.UserName = this.T_User_Name.Text;
            login.Password = this.T_User_Password.Password;

            var l =  Login(login);
        }

        private void button_register_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show();
            Registration register = new Registration();
            register.Show();

            this.Close();
        }


        private async Task Login(Login log)
        {
            Dashboard dashboard = new Dashboard();

            try
            {
                HttpResponseMessage responseMessage = await ApiCall.APIUrl.PostAsJsonAsync("/api/auth/login", log);

                var t = await responseMessage.Content.ReadAsStringAsync();
                if (responseMessage.IsSuccessStatusCode)
                {
                    var authData = JsonConvert.DeserializeObject<AuthData>(t);
                    dashboard.hidToken.Text = authData.Token;
                    dashboard.L_User_Name.Content = authData.UserName;
                    dashboard.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Something is wrong");
                }
            }
            catch (System.Exception e)
            {

                throw;
            }

            
        }
    }
}

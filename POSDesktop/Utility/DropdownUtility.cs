﻿using Newtonsoft.Json;

using POSDesktop.Models;

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace POSDesktop.Utility
{
    public class DropdownUtility
    {
        ApiCallModel ApiCall;
        public DropdownUtility(string token)
        {
            ApiCall = new ApiCallModel(token);
        }

        public async Task<List<Brand>> BrandList()
        {
            List<Brand> BrandListDd = null;
            HttpResponseMessage responseMessage = await ApiCall.APIUrl.GetAsync("/api/Brand");
            var list = await responseMessage.Content.ReadAsStringAsync();
            if (responseMessage.IsSuccessStatusCode)
            {
                BrandListDd = JsonConvert.DeserializeObject<List<Brand>>(list);
            }

            return BrandListDd;
        }

        public async Task<List<Category>> CategoryList()
        {
            List<Category> CategoryListDd = null;

            HttpResponseMessage responseMessage = await ApiCall.APIUrl.GetAsync("/api/Category");
            var list = await responseMessage.Content.ReadAsStringAsync();
            if (responseMessage.IsSuccessStatusCode)
            {
                CategoryListDd = JsonConvert.DeserializeObject<List<Category>>(list);
            }

            return CategoryListDd;

        }

        public async Task<List<Category>> CategoryList(int id)
        {
            List<Category> CategoryListDd = null;

            HttpResponseMessage responseMessage = await ApiCall.APIUrl.GetAsync("/api/Category");
            var list = await responseMessage.Content.ReadAsStringAsync();
            if (responseMessage.IsSuccessStatusCode)
            {
                CategoryListDd = JsonConvert.DeserializeObject<List<Category>>(list);
            }

            return CategoryListDd;

        }

        private async Task<List<SubCategory>> SubCategoryList()
        {
            List<SubCategory> SubCategoryListDd = null;

            HttpResponseMessage responseMessage = await ApiCall.APIUrl.GetAsync("/api/Category");
            var list = await responseMessage.Content.ReadAsStringAsync();
            if (responseMessage.IsSuccessStatusCode)
            {
                SubCategoryListDd = JsonConvert.DeserializeObject<List<SubCategory>>(list);
            }

            return SubCategoryListDd;

        }

        public async Task<List<Supplier>> SupplierList()
        {
            List<Supplier> SupplierListDd = null;
            HttpResponseMessage responseMessage = await ApiCall.APIUrl.GetAsync("/api/Supplier");
            var list = await responseMessage.Content.ReadAsStringAsync();
            if (responseMessage.IsSuccessStatusCode)
            {
                SupplierListDd = JsonConvert.DeserializeObject<List<Supplier>>(list);
            }

            return SupplierListDd;
        }

        public async Task<List<SubCategory>> SubCategoryList(int id)
        {
            List<SubCategory> SubCategoryListDd = null;

            HttpResponseMessage responseMessage = await ApiCall.APIUrl.GetAsync($"/api/DropdownUtility/SubCategoryDropdownListByCategoryId/{id}");
            var list = await responseMessage.Content.ReadAsStringAsync();
            if (responseMessage.IsSuccessStatusCode)
            {
                SubCategoryListDd = JsonConvert.DeserializeObject<List<SubCategory>>(list);
            }

            return SubCategoryListDd;

        }
    }
}

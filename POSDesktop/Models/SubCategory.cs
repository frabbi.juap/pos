﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POSDesktop.Models
{
   public class SubCategory
    {
        public int Id { get; set; }
        public string SubCategoryName { get; set; }
         
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
 
        public int BrandId { get; set; }
        public string BrandName { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace POSDesktop.Models
{
    public class Login
    {
        public int Id { get; set; }
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }
    }

    public class AuthData
    {
        public string Token { get; set; }
        public string UserName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSDesktop.Models
{
    public class SaleProductMaster
    {
        public int Id { get; set; }
         
        public string Voucher{ get; set; }
         
        public List<SaleProductDetail> SaleProducts { get; set; }
    }

    public class SaleProductDetail
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int VoucherId { get; set; }
        public string ProductName { get; set; }
        public double SaleQuantity { get; set; }
        public double SalePrice { get; set; }
        public double TotalPrice { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POSDesktop.Models
{
    public class ProductMasterEntry /*: UtilityBase*/
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public List<ProductDetailEntry> ProductDetails { get; set; }
    }


    public class ProductDetailEntry /*: UtilityBase*/
    {
        public int ProductMasterId { get; set; }
        public int ProductDetailId { get; set; }
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public int SubCategoryId { get; set; }
        public string SubCategoryName { get; set; }
        public string Name { get; set; }
        public double Amount { get; set; }
        public double Price { get; set; }

    }

    public class ProductAutoComplete /*: UtilityBase*/
    {
        public int Id { get; set; }
  
        public string Name { get; set; }

        public Double Price { get; set; }


    }
}

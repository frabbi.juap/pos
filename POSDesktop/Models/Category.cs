﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POSDesktop.Models
{
  public  class Category
    {
        public int Id { get; set; }
        public int BrandId { get; set; }
        public string CategoryName { get; set; }
        public string BrandName { get; set; }
    }
}

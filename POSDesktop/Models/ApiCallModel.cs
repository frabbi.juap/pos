﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace POSDesktop.Models
{
    public class ApiCallModel
    {
        //public string Token;
        public HttpClient APIUrl = new HttpClient();
        public ApiCallModel(string Token)
        {
            APIUrl.BaseAddress = new Uri("https://localhost:44394");
            if (Token != null)
            {
                APIUrl.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            }

        }

    }
}

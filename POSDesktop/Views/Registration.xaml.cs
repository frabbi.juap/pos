﻿using Newtonsoft.Json.Linq;

using POSDesktop.Models;

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POSDesktop.Views
{
    /// <summary>
    /// Interaction logic for Registration.xaml
    /// </summary>
    public partial class Registration : Window
    {
        ApiCallModel ApiCall;
        string token;
        public Registration()
        {
            ApiCall = new ApiCallModel(token);
            InitializeComponent();
        }

       

        private async void button_register_Click(object sender, RoutedEventArgs e)
        {
            Register register = new Register();
            register.FullName = this.T_User_Name.Text;
            register.UserName = this.T_User_Name.Text;
            register.Password = this.T_User_Password.Password;
            register.PhoneNo = this.T_Phone_No.Text;
            register.EmailId = this.T_Email.Text;
            //register.BusinessTypeId = this.T_BusinessTypeId.Text;

             await this.UserRegistration(register);
        }


        private void button_cancel_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show();
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();

            this.Close();
        }

        #region api call

        private async Task UserRegistration(Register register)
        {
            HttpResponseMessage responseMessage = await ApiCall.APIUrl.PostAsJsonAsync("/api/Auth/register", register);

            if (responseMessage.IsSuccessStatusCode)
            {
                MessageBox.Show("Brand inserted");
            }

                
        }
        #endregion
    }
}

﻿using Newtonsoft.Json;

using POSDesktop.Models;

using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace POSDesktop.Views
{
    /// <summary>
    /// Interaction logic for CategoryEntry.xaml
    /// </summary>
    public partial class CategoryEntry : Page
    {
        ApiCallModel ApiCall;
         Category category = new Category();

        List<Category> List { get; set; }
        List<Brand> BrandListDd { get; set; }
        public CategoryEntry(string token)
        {
            ApiCall = new ApiCallModel(token);
            //ApiCall.Token = token;
            InitializeComponent();
            BrandList();
            CategoryList();
        }

     
        private void button_save_Click(object sender, RoutedEventArgs e)
        { 
           category.CategoryName = this.T_Category_Name.Text;
            category.BrandId = (int)this.DD_Brand_Name.SelectedValue;
            SaveCategory(category);
        }


        //private void button_cancel_Click(object sender, RoutedEventArgs e)
        //{
        //    //MessageBox.Show();
        //    Dashboard dashboard = new Dashboard();
             
        //}

        public async Task SaveCategory (Category category)
        {
            HttpResponseMessage responseMessage = await ApiCall.APIUrl.PostAsJsonAsync("/api/Category", category);
            var message = responseMessage.Content.ReadAsStringAsync();

            if (responseMessage.IsSuccessStatusCode)
            {
                MessageBox.Show("Category inserted");
            }
            else
            {
                MessageBox.Show("");
            }                
        }

        private async Task CategoryList()
        {
            HttpResponseMessage responseMessage = await ApiCall.APIUrl.GetAsync("/api/Category");
            var list = await responseMessage.Content.ReadAsStringAsync();
            if (responseMessage.IsSuccessStatusCode)
            {
                List = JsonConvert.DeserializeObject<List<Category>>(list);
            }

            this.CategoryGrid.ItemsSource = List;

        }

        private async Task BrandList()
        {
            HttpResponseMessage responseMessage = await ApiCall.APIUrl.GetAsync("/api/Brand");
            var list = await responseMessage.Content.ReadAsStringAsync();
            if (responseMessage.IsSuccessStatusCode)
            {
                BrandListDd = JsonConvert.DeserializeObject<List<Brand>>(list);
            }

          DataContext=  this.DD_Brand_Name.ItemsSource = BrandListDd;
        }
    }
}

﻿using Newtonsoft.Json;

using POSDesktop.Models;

using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace POSDesktop.Views
{
    /// <summary>
    /// Interaction logic for BrandEntry.xaml
    /// </summary>
    public partial class BrandEntry : Page
    {
        ApiCallModel ApiCall;
        Brand brand = new Brand();
        List<Brand> List { get; set; }
        public BrandEntry(string token)
        {
            ApiCall = new ApiCallModel(token);
            InitializeComponent();
            _ = BrandList();
        }

        private void button_save_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Registration button  clicked");
            brand.Name = this.T_Brand_Name.Text;

            _ = SaveBrand(brand);
           
            this.T_Brand_Name.Text = "";
        }


        //private void button_cancel_Click(object sender, RoutedEventArgs e)
        //{
        //    //MessageBox.Show();
        //    Dashboard dashboard = new Dashboard();
            
        //}

        private async Task SaveBrand(Brand brand)
        {
            HttpResponseMessage responseMessage = await ApiCall.APIUrl.PostAsJsonAsync("/api/Brand", brand);

            if (responseMessage.IsSuccessStatusCode) {
                _ = BrandList();
                MessageBox.Show("Brand inserted");
            }
                
        }

        private async Task BrandList()
        {
            HttpResponseMessage responseMessage = await ApiCall.APIUrl.GetAsync("/api/Brand");
            var list = await responseMessage.Content.ReadAsStringAsync();
            if (responseMessage.IsSuccessStatusCode)
            {
                List = JsonConvert.DeserializeObject<List<Brand>>(list);
            }

            this.BrandGrid.ItemsSource = List;
        }
    }
}

﻿using Newtonsoft.Json;

using POSDesktop.Models;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace POSDesktop.Views
{
    /// <summary>
    /// Interaction logic for SaleProduct.xaml
    /// </summary>
    public partial class ProductSale : Page
    {
        ApiCallModel ApiCall;
        ObservableCollection<SaleProductDetail> list;
        private int voucherId; 
        SaleProductMaster saleProductMaster;

        public ProductSale(string token)
        {
            ApiCall = new ApiCallModel(token);
            saleProductMaster = new SaleProductMaster();
            saleProductMaster.SaleProducts = new List<SaleProductDetail>();
            _ = SoldProducts();
            _ = GetVoucherNo();
            InitializeComponent();
            list = new ObservableCollection<SaleProductDetail>();
            DataContext = this;
        }


        #region events

        private void Add_Product(object sender, RoutedEventArgs e)
        {
            SaleProductDetail saleProd = new SaleProductDetail();

            saleProd.ProductId = Convert.ToInt32(product_id.Text);
            saleProd.ProductName = this.product_name.Text;
            saleProd.SaleQuantity = Convert.ToDouble(this.quantity.Text);
            saleProd.SalePrice = Convert.ToDouble(this.price.Text);
            saleProd.TotalPrice = saleProd.SalePrice * saleProd.SaleQuantity;

            list.Add(saleProd);
            saleProductMaster.SaleProducts.Add(saleProd);

            SaleProductTemp.ItemsSource = list;

            product_id.Text = null;
            product_name.Text = null;
            quantity.Text = null;
            price.Text = null;

            _ = product_name.Focus();

        }

        private void Product_Name_Autocomple(object sender, RoutedEventArgs e)
        {
            string name = this.product_name.Text;
            if (!string.IsNullOrWhiteSpace(name))
            {
                _ = SearchProduct(name);
            }
            else
            {
                this.product_name_auto_com.ItemsSource = null;
            }
        }

        private void Select_Product_Mouse_Click(object sender, RoutedEventArgs e)
        {
            var item = ItemsControl.ContainerFromElement(sender as ListBox, e.OriginalSource as DependencyObject) as ListBoxItem;
            if (item != null)
            {
                ProductAutoComplete productDetail = (ProductAutoComplete)item.Content;

                this.product_id.Text = productDetail.Id.ToString();
                this.product_name.Text = productDetail.Name;
                this.price.Text = productDetail.Price.ToString();

                this.product_name_auto_com.ItemsSource = null;
                // ListBox item clicked - do some cool things here
            }
        }

        private void Save_Button(object sender, RoutedEventArgs e)
        {
            if (saleProductMaster.SaleProducts != null)
            {
                saleProductMaster.Voucher = (string)this.Label_Voucher.Content;
                _ = SellProduct(saleProductMaster);
                _ = SoldProducts();

                SaleProductTemp.ItemsSource = null;
            }

            _ = product_name.Focus();

        }

        private void Button_Sale_Detail_List(object sender, RoutedEventArgs e)
        {
            int voucherId = this.voucherId = (int)((Button)sender).CommandParameter;
            _ = SoldProductDetailList(voucherId);
            this.Popup1.IsOpen = true;

           
        }

        private void Close_Popup_ProductDetailList(object sender, RoutedEventArgs e)
        {
            this.Popup1.IsOpen = false;
        }

        private void Button_Delete_Sale_Product(object sender, RoutedEventArgs e)
        {
            int Id = (int)((Button)sender).CommandParameter;

            _ = DeleteSoldProduct(Id);

            _ = SoldProductDetailList(voucherId);
            this.Popup1.IsOpen = true;
        }
        #endregion

        #region apicall

        public async Task GetVoucherNo()
        {
            try
            {
                SaleProductMaster voucherSales = new SaleProductMaster();
                HttpResponseMessage responseMessage = await ApiCall.APIUrl.GetAsync($"/api/VoucherGeneration");
                var voucher = await responseMessage.Content.ReadAsStringAsync();

                if (responseMessage.IsSuccessStatusCode)
                {
                    //var v = JsonConvert.DeserializeObject<string>(voucher);
                    this.Label_Voucher.Content = voucher;
                }
                else
                {
                    MessageBox.Show("");
                }

                //return voucherSales.Voucher;
            }
            catch (Exception e)
            {

                throw;
            }


        }

        public async Task SearchProduct(string name)
        {
            List<ProductAutoComplete> productDetails = new List<ProductAutoComplete>();
            HttpResponseMessage responseMessage = await ApiCall.APIUrl.GetAsync($"/api/SearchProduct/name?name={name}");
            var list = await responseMessage.Content.ReadAsStringAsync();

            if (responseMessage.IsSuccessStatusCode)
            {
                productDetails = JsonConvert.DeserializeObject<List<ProductAutoComplete>>(list);
                this.product_name_auto_com.ItemsSource = productDetails;
            }
            else
            {
                MessageBox.Show("");
            }
        }

        public async Task SellProduct(SaleProductMaster sale)
        {
            HttpResponseMessage responseMessage = await ApiCall.APIUrl.PostAsJsonAsync($"/api/ProductSale", sale);
            var message = await responseMessage.Content.ReadAsStringAsync();

            if (responseMessage.IsSuccessStatusCode)
            {
                MessageBox.Show("Product Sold.");
            }
            else
            {
                MessageBox.Show("");
            }

        }

        public async Task SoldProducts()
        {
            try
            {
                HttpResponseMessage responseMessage = await ApiCall.APIUrl.GetAsync($"/api/ProductSale");
                var list = await responseMessage.Content.ReadAsStringAsync();

                if (responseMessage.IsSuccessStatusCode)
                {
                    List<SaleProductMaster> SaleProduct = JsonConvert.DeserializeObject<List<SaleProductMaster>>(list);
                    SoldProduct_List.ItemsSource = SaleProduct;
                }
                else
                {
                    MessageBox.Show("No Data Found");
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public async Task SoldProductDetailList(int voucherId)
        {
            try
            {
                HttpResponseMessage responseMessage = await ApiCall.APIUrl.GetAsync($"/api/ProductSale/id?id={voucherId}");
                var productList = responseMessage.Content.ReadAsStringAsync();

                if (responseMessage.IsSuccessStatusCode)
                {
                    List<SaleProductDetail> detailList = JsonConvert.DeserializeObject<List<SaleProductDetail>>(await productList);
                    this.ProductDetailListPop.ItemsSource = detailList;
                }
                else
                {
                    MessageBox.Show("no data found");
                }
            }
            catch (Exception e)
            {

                throw;
            }


        }

        public async Task DeleteSoldProduct(int id)
        {
            try
            {
                HttpResponseMessage responseMessage = await ApiCall.APIUrl.DeleteAsync($"/api/ProductSale/id?id={id}");
                var message = await responseMessage.Content.ReadAsStringAsync();

                if (responseMessage.IsSuccessStatusCode)
                {
                    MessageBox.Show("Successfully Deleted!");
                }
                else
                {
                    MessageBox.Show("No Data Found");
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }
        #endregion
    }
}

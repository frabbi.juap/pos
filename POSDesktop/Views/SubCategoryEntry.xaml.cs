﻿using Newtonsoft.Json;

using POSDesktop.Models;
using POSDesktop.Utility;

using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace POSDesktop.Views
{
    /// <summary>
    /// Interaction logic for SubCategoryEntry.xaml
    /// </summary>
    public partial class SubCategoryEntry : Page
    {
        ApiCallModel ApiCall;
        DropdownUtility dropdown;
        SubCategory subCategory = new SubCategory();

        List<Category> CategoryListDd { get; set; }
        List<SubCategory> SubCategories { get; set; }
        List<Brand> BrandListDd { get; set; }

        public SubCategoryEntry(string token)
        {
            ApiCall = new ApiCallModel(token);
            dropdown = new DropdownUtility(token);
            InitializeComponent();
            SubCategoryList();
            BrandList();
            CategoryList();
        }


        private void button_save_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Registration button  clicked");
            subCategory.SubCategoryName = this.T_Sub_Category_Name.Text;
            subCategory.BrandId = (int)this.DD_Brand_Name.SelectedValue;
            subCategory.CategoryId = (int)this.DD_Category_Name.SelectedValue;

            SaveSubCategory(subCategory);
             
        }

        public async Task SaveSubCategory(SubCategory subCategory)
        {
            HttpResponseMessage responseMessage = await ApiCall.APIUrl.PostAsJsonAsync("/api/SubCategory", subCategory);
            var message = responseMessage.Content.ReadAsStringAsync();

            if (responseMessage.IsSuccessStatusCode)
            {
                MessageBox.Show("Sub Category inserted");
            }
            else
            {
                MessageBox.Show(message.Status.ToString());
            }
        }

        private async Task SubCategoryList()
        {
            HttpResponseMessage responseMessage = await ApiCall.APIUrl.GetAsync("/api/SubCategory");
            var list = await responseMessage.Content.ReadAsStringAsync();
            if (responseMessage.IsSuccessStatusCode)
            {
                SubCategories = JsonConvert.DeserializeObject<List<SubCategory>>(list);
            }

            this.SubCategoryGrid.ItemsSource = SubCategories;

        }


        private void button_cancel_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show();
            Dashboard dashboard = new Dashboard();
             
        }

        private async Task BrandList()
        { 
            this.DD_Brand_Name.ItemsSource = await dropdown.BrandList() ;// BrandListDd;
        }

        private async Task CategoryList()
        { 
            this.DD_Category_Name.ItemsSource = await dropdown.CategoryList(); //CategoryListDd;

        }
    }
}

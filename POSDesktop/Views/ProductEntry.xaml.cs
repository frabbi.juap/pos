﻿using Newtonsoft.Json;

using POSDesktop.Models;
using POSDesktop.Utility;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace POSDesktop.Views
{
    /// <summary>
    /// Interaction logic for ItemEntry.xaml
    /// </summary>
    public partial class ItemEntry : Page
    {
        private readonly DropdownUtility dropdown;
        private readonly ProductMasterEntry productMaster;
        

        ApiCallModel ApiCall;
        private readonly ObservableCollection<ProductDetailEntry> list;
        public ItemEntry(string token)
        {
            ApiCall = new ApiCallModel(token);
            productMaster = new ProductMasterEntry();
            
            productMaster.ProductDetails = new List<ProductDetailEntry>();
            dropdown = new DropdownUtility(token);
            list = new ObservableCollection<ProductDetailEntry>();
            InitializeComponent();

            _ = CategoryListDropdown();
            _ = SupplierListDropdown();
            _ = ProductList();
            this.DataContext = this;
        }


        private async Task CategoryListDropdown()
        {
            this.DD_Category_Name.ItemsSource = await dropdown.CategoryList(); //CategoryListDd;

        }

        private async Task SubCategoryListDropdown(int id)
        {
            this.DD_Sub_Category_Name.ItemsSource = await dropdown.SubCategoryList(id);
        }

        private async Task SupplierListDropdown()
        {
            this.DD_Supplier_Name.ItemsSource = await dropdown.SupplierList(); //CategoryListDd;

        }

        #region button event

        private void button_add_details(object sender, RoutedEventArgs e)
        {
            if (this.DD_Sub_Category_Name.SelectedValue != null)
            {
                ProductDetailEntry productDetail = new ProductDetailEntry();

                productDetail.SubCategoryId = (int)this.DD_Sub_Category_Name.SelectedValue;
                productDetail.SubCategoryName = this.DD_Sub_Category_Name.Text;
                productDetail.SupplierId = (int)this.DD_Supplier_Name.SelectedValue;
                productDetail.SupplierName = this.DD_Supplier_Name.Text;
                productDetail.Name = this.T_Product_Name.Text;
                productDetail.Amount = Convert.ToDouble(this.T_Amount.Text);
                productDetail.Price = Convert.ToDouble(this.T_Price.Text);

                if (productDetail.SubCategoryId != 0 && productDetail.SupplierId != 0)
                {
                    list.Add(productDetail);
                    productMaster.ProductDetails.Add(productDetail);
                    this.ProductDetailsList.ItemsSource = list;
                    MessageBox.Show("added to list");

                    //this.DD_Category_Name.SelectedValue = 0;
                    //this.DD_Sub_Category_Name.SelectedValue = 0;
                    //this.DD_Supplier_Name.SelectedValue = 0;

                    this.T_Product_Name.Text = "";
                    this.T_Price.Text = "";
                    this.T_Amount.Text = "";
                }
                else
                {
                    MessageBox.Show("Please select sub category and supplier");

                }
            }

        }

        private void Save_Button(object sender, RoutedEventArgs e)
        {
            productMaster.CategoryId = (int)this.DD_Category_Name.SelectedValue;

            SaveProduct(productMaster);
            ProductList();
        }

        private void Button_Product_Detail_List(object sender, RoutedEventArgs e)
        {
            int id = (int)((Button)sender).CommandParameter;

            this.Popup1.IsOpen = true;

            ProductDetailList(id);
        }

        private void Close_Popup_ProductDetailList(object sender, RoutedEventArgs e)
        {
            this.Popup1.IsOpen = false;
        }

        private void ComboBox_OnChangeSubCategory(object sender, SelectionChangedEventArgs e)
        {
            int catid = (int)this.DD_Category_Name.SelectedValue;

            SubCategoryListDropdown(catid);
        }

        #endregion

        #region apicall

        public async Task SaveProduct(ProductMasterEntry ProductMasterEntry)
        {
            HttpResponseMessage responseMessage = await ApiCall.APIUrl.PostAsJsonAsync("/api/ProductEntry", ProductMasterEntry);
            var message = responseMessage.Content.ReadAsStringAsync();

            if (responseMessage.IsSuccessStatusCode)
            {
                MessageBox.Show("Category inserted");
            }
            else
            {
                MessageBox.Show("");
            }
        }

        public async Task<List<ProductMasterEntry>> ProductList()
        {
            List<ProductMasterEntry> list = new List<ProductMasterEntry>();
            HttpResponseMessage responseMessage = await ApiCall.APIUrl.GetAsync("/api/ProductEntry");
            var productList = responseMessage.Content.ReadAsStringAsync();

            if (responseMessage.IsSuccessStatusCode)
            {
                list = JsonConvert.DeserializeObject<List<ProductMasterEntry>>(await productList);
                this.AllProductList.ItemsSource = list;
            }
            else
            {
                MessageBox.Show("");
            }
            return list;
        }

        public async Task<List<ProductDetailEntry>> ProductDetailList(int id)
        {
            List<ProductDetailEntry> detailList = new List<ProductDetailEntry>();

            try
            {
                HttpResponseMessage responseMessage = await ApiCall.APIUrl.GetAsync($"/api/ProductEntry/id?id={id}");
                var productList = responseMessage.Content.ReadAsStringAsync();

                if (responseMessage.IsSuccessStatusCode)
                {
                    detailList = JsonConvert.DeserializeObject<List<ProductDetailEntry>>(await productList);
                    this.ProductDetailListPop.ItemsSource = detailList;
                }
                else
                {
                    MessageBox.Show("");
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return detailList;
        }


        #endregion
    }
}

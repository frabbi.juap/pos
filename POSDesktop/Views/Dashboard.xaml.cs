﻿using LiveCharts;
using LiveCharts.Wpf;

using System.Windows;

namespace POSDesktop.Views
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : Window
    {

        public string Token { get; set; }
        public Dashboard()
        {
            InitializeComponent();
            Token = hidToken.Text;
        }

        private void button_brandEntry_click(object sender, RoutedEventArgs e)
        {
            Token = this.hidToken.Text;
            this.POSFrame.Content = new BrandEntry(Token);
        }

        private void button_CategoryEntry_click(object sender, RoutedEventArgs e)
        {
            Token = this.hidToken.Text;
            this.POSFrame.Content = new CategoryEntry(Token);
            //category.Show();

        }

        private void button_SubCategoryEntry_click(object sender, RoutedEventArgs e)
        {
            Token = this.hidToken.Text;
            this.POSFrame.Content = new SubCategoryEntry(Token);

        }

        private void button_ItemEntry_click(object sender, RoutedEventArgs e)
        {
            Token = this.hidToken.Text;
            this.POSFrame.Content = new ItemEntry(Token);
            //itemEntry.Show();

        }

        private void button_Product_Sale_click(object sender, RoutedEventArgs e)
        {
            Token = this.hidToken.Text;
            this.POSFrame.Content = new ProductSale(Token);
            //itemEntry.Show();

        }

        private void button_logout_click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            this.Close();
            mainWindow.Show();
        }   


        //private void charts()
        //{
        //    SeriesCollection obj = new SeriesCollection
        //    {
        //        new LineSeries
        //        {
        //            Values = new ChartValues<double> { 3, 5, 7, 4 }
        //        },
        //        new ColumnSeries
        //        {
        //            Values = new ChartValues<decimal> { 5, 6, 2, 7 }
        //        }
        //    };


        //}

    }
}

﻿using POSDesktop.Models;

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace POSDesktop.Views
{
    /// <summary>
    /// Interaction logic for BrandEntry_1.xaml
    /// </summary>
    public partial class BrandEntry_1 : Page
    {
        public BrandEntry_1()
        {
            InitializeComponent();
        }

        Brand brand = new Brand();

        private void button_save_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Registration button  clicked");
            brand.BrandName = this.T_Brand_Name.Text;

        }


        //private void button_cancel_Click(object sender, RoutedEventArgs e)
        //{
        //    //MessageBox.Show();
        //    Dashboard dashboard = new Dashboard();
        //    dashboard.Show();            
        //}
    }
}

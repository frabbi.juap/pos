﻿using POSDesktop.Models;
using POSDesktop.Views;

using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace POSDesktop.Pages
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        Login login = new Login();
        private void button_login_Click(object sender, RoutedEventArgs e)
        {
            login.UserName = this.T_User_Name.Text;
            login.Password = this.T_User_Password.Password;

            var l = UserLogin(login);
        }

        private void button_register_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show();
            Registration register = new Registration();
            register.Show();

        }


        private async Task UserLogin(Login log)
        {
            //Dashboard dashboard = new Dashboard();
            HttpClient api = new HttpClient();
            api.BaseAddress = new Uri("https://localhost:44394/");

            HttpResponseMessage responseMessage = await api.PostAsJsonAsync("api/auth/login", login);

            if (responseMessage.IsSuccessStatusCode) 
            { 
           
                    }
                

        }
    }
}

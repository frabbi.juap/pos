﻿using Infrastructure.IServices.IAuthService;
using Infrastructure.IServices.IHelperService;
using Infrastructure.IServices.IMasterService;
using Infrastructure.IServices.ISettingService;
using Infrastructure.Security;
using Infrastructure.Services.AuthServices;
using Infrastructure.Services.Helper;
using Infrastructure.Services.MasterService;
using Infrastructure.Services.SettingService;

using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Common
{
    public static  class ServiceInjection
    {
        public static IServiceCollection AddInfrustructure(this IServiceCollection services)
        {

            services.AddScoped<IRegisterService, RegisterService>();
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IRoleService, RoleService>();

            services.AddScoped<ITokenGenerator, TokenGenerator>();
            services.AddScoped<IDateFormat, DateFormat>();
            services.AddSingleton<IUserAccessor, UserAccessor>();

            services.AddTransient<IBrandService, BrandService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<ISubCategoryService, SubCategoryService>();
            services.AddTransient<ISupplierService, SupplierService>();

            services.AddTransient<IProductEntryService, ProductEntryService>();
            services.AddTransient<IProductSearchService, SearchProductService>();
            services.AddTransient<IProductSaleService,ProductSaleService>();

            services.AddTransient<IDropdownUtilityService, DropdownUtilityService>();
            services.AddTransient<IGenerateVoucherService, GenerateVoucherService>();

            return services;
        }
    }
}

﻿using Models.Master.DTO;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.IServices.IMasterService
{
    public interface IProductEntryService
    {
        Task<bool> CreateProduct(ProductMasterEntryDto productMaster);
        Task<List<ProductMasterEntryDto>> ProductList();
        Task<List<ProductDetailEntryDto>> ProductsById(int productId);
    }
}

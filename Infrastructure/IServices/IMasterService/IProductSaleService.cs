﻿using Models.Master.DTO;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.IServices.IMasterService
{
    public interface IProductSaleService
    {
        Task<bool> SellProduct(ProductSaleMasterDto productSaleDto);
        Task<List<ProductSaleMasterDto>> SoldProductList();
        Task<List<ProductSaleDetailDto>> SoldProductsById(int voucherId);
        Task<bool> DeleteSoldProduct(int id);
    }
}

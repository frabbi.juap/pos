﻿using Models.Settings;
using Models.Settings.DTO;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.IServices.ISettingService
{
    public interface ISupplierService
    {
        Task<List<SupplierDto>> SupplierList();
        Task<bool> CreateSupplier(SupplierDto supplierDto);
        Task<bool> DeleteSupplier(int id);
    }
}

﻿using Models.Settings.DTO;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.IServices.ISettingService
{
   public interface ISubCategoryService
    {
        Task<List<SubCategoryDto>> SubCategoryList();
        Task<bool> CreateSubCategory(SubCategoryDto subCategoryDto);
        Task<bool> DeleteSubCategory(int id);
    }
}

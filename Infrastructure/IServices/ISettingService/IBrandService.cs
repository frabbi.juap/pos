﻿using Models.Settings;
using Models.Settings.DTO;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.IServices.ISettingService
{
    public interface IBrandService
    {
        Task<bool> CreateBrand(BrandDto brand);

        Task<BrandDto> BrandById(int brandId);

        Task<bool> DeleteBrand(int brandId);

        Task<List<BrandDto>> BrandList();
    }
}

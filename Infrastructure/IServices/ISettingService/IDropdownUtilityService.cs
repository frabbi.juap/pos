﻿using Models.Settings.DTO;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.IServices.ISettingService
{
   public interface IDropdownUtilityService
    {
        Task<List<CategoryDto>> CategoryListByBrandId(int brandId);
        Task<List<SubCategoryDto>> SubCategoryListByCategoryId(int categoryId);
       
    }
}

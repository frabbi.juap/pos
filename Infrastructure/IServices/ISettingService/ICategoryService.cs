﻿using Models.Settings;
using Models.Settings.DTO;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.IServices.ISettingService
{
    public interface ICategoryService
    {
        Task<bool> CreateCategory(CategoryDto category);

        Task<CategoryDto> CategoryById(int categoryId);

        Task<bool> DeleteCategory(int categoryId);

        Task<List<CategoryDto>> CategoryList();
    }
}

﻿using Infrastructure.IServices.IHelperService;

using Microsoft.EntityFrameworkCore;

using Persistance;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Services.Helper
{
    public class GenerateVoucherService : IGenerateVoucherService
    {
        private readonly POSContext _context;

        public GenerateVoucherService(POSContext context)
        {
            this._context = context;
        }

        public async Task<string> GenerateVoucher()
        {
            var voucherCount = await _context.SaleMaster.CountAsync() +1;

            string voucherNo = $"Voucher-{DateTime.Today.ToShortDateString()}-" + voucherCount;

            return voucherNo;
        }
    }
}

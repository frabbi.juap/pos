﻿using AutoMapper;

using Infrastructure.IServices.IHelperService;

using Microsoft.EntityFrameworkCore;

using Models.Master.DTO;

using Persistance;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Services.Helper
{
    public class SearchProductService : IProductSearchService
    {
        private readonly POSContext _context;
        private readonly IMapper _mapper;

        public SearchProductService(POSContext context, IMapper mapper)
        {
            _context = context;
           _mapper = mapper;
        }

        public async Task<List<ProductDetailEntryDto>> SearchProducts(string searchProductName)
        {
            var products = await (from d in _context.ProductDetail 
                                  join s in _context.Supplier on d.SupplierId equals s.Id
                            where d.Name.Contains($"{searchProductName}")
                            select new ProductDetailEntryDto
                            {
                                Id = d.Id,
                                Name = d.Name,
                                Amount = d.Amount,
                                Price = d.Price,
                                SupplierName = s.Name,
                                
                            }).ToListAsync();

            return   _mapper.Map<List<ProductDetailEntryDto>>(products);

        }
    }
}

﻿using AutoMapper;

using Infrastructure.Common;
using Infrastructure.IServices.IHelperService;
using Infrastructure.IServices.IMasterService;
using Infrastructure.Security;

using Microsoft.EntityFrameworkCore;

using Models.Master;
using Models.Master.DTO;

using Persistance;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Services.MasterService
{

    public class ProductEntryService : IProductEntryService
    {
        private readonly POSContext _context;
        private readonly IDateFormat _dateFormat;
        private readonly IUserAccessor _userAccessor;
        private readonly IMapper _mapper;

        public ProductEntryService(POSContext context, IDateFormat dateFormat, IUserAccessor userAccessor, IMapper mapper)
        {
            _context = context;
            _dateFormat = dateFormat;
            _userAccessor = userAccessor;
            _mapper = mapper;
        }

        public async Task<bool> CreateProduct(ProductMasterEntryDto productMaster)
        {
            try
            {

                var t = _context.ProductMaster.Any(pm => pm.CategoryId == productMaster.CategoryId);
                if (!_context.ProductMaster.Any(pm => pm.CategoryId == productMaster.CategoryId))
                {
                    if (productMaster.Id == 0)
                    {
                        var product = new ProductMasterEntry
                        {
                            CategoryId = productMaster.CategoryId,
                            PostedBy = _userAccessor.Id,
                            InsertDate = _dateFormat.Now
                        };

                        _context.ProductMaster.Add(product);
                        var resultMaster = await _context.SaveChangesAsync();

                        var masterId = product.Id;

                        foreach (var details in productMaster.ProductDetails)
                        {
                            var productDetail = new ProductDetailEntry
                            {
                                ProductMasterId = masterId,
                                SupplierId = details.SupplierId,
                                SubCategoryId = details.SubCategoryId,
                                Name = details.Name,
                                Amount = details.Amount,
                                Price = details.Price,
                                PostedBy = _userAccessor.Id,
                                InsertDate = _dateFormat.Now
                            };
                            _context.ProductDetail.Add(productDetail);
                        }

                        //_context.ProductDetail.AddRange(_mapper.Map<List<ProductDetailEntry>>(productMaster.ProductDetails));

                        var result = await _context.SaveChangesAsync() > 0;

                        return result;
                    }
                }

                return true;
            }
            catch (Exception e)
            {

                throw;
            }

        }

        public async Task<List<ProductDetailEntryDto>> ProductsById(int productId)
        {
            var productDetail = await (from pm in _context.ProductMaster
                                       join pd in _context.ProductDetail on pm.Id equals
                                       pd.ProductMasterId

                                       join subCat in _context.SubCategory on pd.SubCategoryId equals subCat.Id
                                       join supplier in _context.Supplier on pd.SupplierId equals supplier.Id

                                       where pm.Id == productId
                                       select new ProductDetailEntryDto
                                       {
                                           ProductMasterId = pm.Id,
                                           SubCategoryId = pd.SubCategoryId,
                                           SubCategoryName = subCat.SubCategoryName,
                                           SupplierId = pd.SupplierId,
                                           SupplierName = supplier.Name,
                                           Name = pd.Name,
                                           Amount = pd.Amount,
                                           Price = pd.Price

                                       }).ToListAsync();

            return _mapper.Map<List<ProductDetailEntryDto>>(productDetail);
        }

        public async Task<List<ProductMasterEntryDto>> ProductList()
        {
            try
            {
                var productList = await (from pm in _context.ProductMaster
                                         join c in _context.Category on pm.CategoryId equals c.Id

                                         select new ProductMasterEntryDto
                                         {
                                             Id = pm.Id,
                                             CategoryName = c.CategoryName
                                         }
                                         ).ToListAsync();

                return _mapper.Map<List<ProductMasterEntryDto>>(productList);
            }
            catch (Exception e)
            {
                throw new RestException(System.Net.HttpStatusCode.NotFound);
            }
        }
    }
}

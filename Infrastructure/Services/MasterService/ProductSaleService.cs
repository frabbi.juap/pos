﻿using AutoMapper;

using Infrastructure.IServices.IHelperService;
using Infrastructure.IServices.IMasterService;

using Microsoft.EntityFrameworkCore;

using Models.Master;
using Models.Master.DTO;

using Persistance;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Master;
using Infrastructure.Common;

namespace Infrastructure.Services.MasterService
{
    public class ProductSaleService : IProductSaleService
    {
        private readonly POSContext _context;
        private readonly IMapper _mapper;
        private readonly IDateFormat _dateFormat;

        public ProductSaleService(POSContext context, IMapper mapper, IDateFormat dateFormat)
        {
            this._context = context;
            this._mapper = mapper;
            this._dateFormat = dateFormat;
        }

        public async Task<bool> DeleteSoldProduct(int id)
        {
            try
            {
                var result = await _context.SaleDetail.Where(x => x.Id == id).SingleAsync();
                _context.SaleDetail.Remove(result);
                _context.SaveChanges();

                return true; 
            }
            catch (Exception)
            {

                throw new RestException(System.Net.HttpStatusCode.BadRequest, "");
            }
        }

        public async Task<bool> SellProduct(ProductSaleMasterDto productSaleDto)
        {
            bool resultMaster = false;
            try
            {
                if (productSaleDto.Id == 0)
                {
                    var saleMaster = new ProductSaleMaster
                    {
                        Voucher = productSaleDto.Voucher
                    };
                    //saleMaster.SaleProducts = _mapper.Map<List<ProductSaleDetail>>(productSaleDto.SaleProducts);
                    _context.SaleMaster.Add(saleMaster);
                    await _context.SaveChangesAsync();

                    int id = saleMaster.Id;

                    if (id != 0)
                    {
                        foreach (var detail in productSaleDto.SaleProducts)
                        {
                            var saleDetail = new ProductSaleDetail
                            {
                                VoucherId = id,
                                ProductId = detail.ProductId,
                                SaleQuantity = detail.SaleQuantity,
                                SalePrice = detail.SalePrice
                            };
                            _context.SaleDetail.Add(saleDetail);
                        }

                    }


                }
                resultMaster = await _context.SaveChangesAsync() > 0;

                return resultMaster;

            }
            catch (Exception ex)
            {

            }
            return resultMaster;
        }

        public async Task<List<ProductSaleMasterDto>> SoldProductList()
        {
            try
            {
                var saleMasterlist = await _context.SaleMaster.ToListAsync();
                //foreach(var master in saleMasterlist)
                // {
                //     var saleDetaillist = await _context.SaleDetail.Where(d => d.VoucherId == master.Id).ToListAsync();

                //     saleMasterlist
                // }

                return _mapper.Map<List<ProductSaleMasterDto>>(saleMasterlist);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<List<ProductSaleDetailDto>> SoldProductsById(int voucherId)
        {
            try
            {
                var details = await _context.SaleDetail
                    .Join(_context.ProductDetail, sale => sale.ProductId, product => product.Id, 
                    (sale, product) => new ProductSaleDetailDto
                    {
                        Id = sale.Id,
                        VoucherId = sale.VoucherId,
                        ProductName = product.Name,
                        SaleQuantity = sale.SaleQuantity,
                        SalePrice = sale.SalePrice,
                        TotalPrice = sale.SaleQuantity * sale.SalePrice
                    }
                    ).Where(x => x.VoucherId == voucherId).ToListAsync();

                return _mapper.Map<List<ProductSaleDetailDto>>(details);
            }
            catch (Exception)
            {

                throw new RestException(System.Net.HttpStatusCode.BadRequest, "");
            }
        }
    }
}

﻿using AutoMapper;

using Infrastructure.Common;
using Infrastructure.IServices.IHelperService;
using Infrastructure.IServices.ISettingService;
using Infrastructure.Security;

using Microsoft.EntityFrameworkCore;

using Models.Settings;
using Models.Settings.DTO;

using Persistance;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Services.SettingService
{
    public class SubCategoryService : ISubCategoryService
    {
        private readonly POSContext _context;
        private readonly IUserAccessor _userAccessor;
        private readonly IDateFormat _dateFormat;
        private readonly IMapper _mapper;

        public SubCategoryService(POSContext context, IUserAccessor userAccessor, IDateFormat dateFormat, IMapper mapper)
        {
            _context = context;
            _userAccessor = userAccessor;
            _dateFormat = dateFormat;
            _mapper = mapper;
        }
        public async Task<bool> CreateSubCategory(SubCategoryDto subCategoryDto)
        {
            try
            {
                if (subCategoryDto.Id == 0)
                {
                    var subCategoryExist = await _context.SubCategory.AnyAsync(sc => sc.SubCategoryName == subCategoryDto.SubCategoryName);

                    if (!subCategoryExist)
                    {
                        var subCategory = new SubCategory
                        {
                            SubCategoryName = subCategoryDto.SubCategoryName,
                            CategoryId = subCategoryDto.CategoryId,
                             

                            PostedBy = _userAccessor.Id,
                            InsertDate = _dateFormat.Now
                        };

                        _context.SubCategory.Add(subCategory);
                    }
                    else
                    {
                        throw new RestException(System.Net.HttpStatusCode.Found);
                    }
                }
                else
                {
                    subCategoryDto.UpdateDate = _dateFormat.Now;
                    subCategoryDto.ModifiedBy = _userAccessor.UserName;

                    _context.Entry(_mapper.Map<SubCategory>(subCategoryDto)).State = EntityState.Modified;
                }

                var result = await _context.SaveChangesAsync() > 0;

                return result;
            }
            catch (Exception e)
            {
                throw new RestException(System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Task<bool> DeleteSubCategory(int id)
        {
            throw new System.NotImplementedException();
        }

        public async Task<List<SubCategoryDto>> SubCategoryList()
        {
            var subCategoryList = await (from sc in _context.SubCategory
                                         join c in _context.Category on sc.CategoryId equals c.Id
                                         join b in _context.Brand on c.BrandId equals b.Id
                                        //join u in _context.MeasurementUnit on sc.UnitId equals u.Id
                                         select new SubCategoryDto
                                         {
                                             Id = sc.Id,
                                             CategoryId = c.Id,
                                             BrandId = b.Id,
                                             //UnitId = u.Id,
                                             SubCategoryName = sc.SubCategoryName, 
                                             CategoryName = c.CategoryName,
                                             BrandName = b.Name,
                                             //UnitName = u.Name
                                         }).ToListAsync();

          

            return _mapper.Map<List<SubCategoryDto>>(subCategoryList);
        }
    }
}

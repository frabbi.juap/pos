﻿using AutoMapper;

using Infrastructure.Common;
using Infrastructure.IServices.IHelperService;
using Infrastructure.IServices.ISettingService;
using Infrastructure.Security;

using Microsoft.EntityFrameworkCore;

using Models.Settings;
using Models.Settings.DTO;

using Persistance;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Services.SettingService
{
    public class BrandService : IBrandService
    {
        private readonly POSContext _context;
        private readonly IDateFormat _dateFormat;
        private readonly IMapper _mapper;
        private readonly IUserAccessor _userAccessor;

        public BrandService(POSContext context, IDateFormat dateFormat, IMapper mapper, IUserAccessor userAccessor)
        {
            _context = context;
            _dateFormat = dateFormat;
            _mapper = mapper;
            _userAccessor = userAccessor;
        }

        public Task<BrandDto> BrandById(int brandId)
        {
            throw new NotImplementedException();
        }

        public async Task<List<BrandDto>> BrandList()
        {
            var brandList = await _context.Brand.ToListAsync();
            return _mapper.Map<List<BrandDto>>(brandList);
        }

        public async Task<bool> CreateBrand(BrandDto brandDto)
        {
            try
            {
                if(brandDto.Id == 0)
                {
                    var brandExists = await _context.Brand.AnyAsync(b => b.Name == brandDto.Name);
                    if (!brandExists)
                    {
                        var brand = new Brand
                        {
                            Name = brandDto.Name,
                            PostedBy = _userAccessor.Id,
                            InsertDate = _dateFormat.Now
                        };                        
                        _context.Brand.Add(brand);
                    }
                    else
                    {
                        throw new RestException(System.Net.HttpStatusCode.Found, "Exist");
                    }
                    
                }
                else
                {                     
                    brandDto.UpdateDate = _dateFormat.Now;
                    brandDto.ModifiedBy = _userAccessor.UserName;

                    _context.Entry(_mapper.Map<Brand>(brandDto)).State = EntityState.Modified;
                    //_context.Update<Brand>(_mapper.Map<Brand>(brandDto));

                    
                }

                var result = await _context.SaveChangesAsync() > 0;

                return result;
            }
            catch (Exception e)
            {

                throw new RestException(System.Net.HttpStatusCode.NotFound, e.Message);
            }
        }

        public Task<bool> DeleteBrand(int brandId)
        {
            throw new NotImplementedException();
        }
    }
}

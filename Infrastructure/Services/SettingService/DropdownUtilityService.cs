﻿using AutoMapper;

using Infrastructure.IServices.ISettingService;

using Microsoft.EntityFrameworkCore;

using Models.Settings.DTO;

using Persistance;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Services.SettingService
{
    public class DropdownUtilityService : IDropdownUtilityService
    {
        private readonly POSContext _context;
        private readonly IMapper _mapper;

        public DropdownUtilityService(POSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<CategoryDto>> CategoryListByBrandId(int brandId)
        {
            var categoryList = await _context.Category.Where(c => c.BrandId == brandId).ToListAsync();

            return  _mapper.Map<List<CategoryDto>>(categoryList);
        }

        public async Task<List<SubCategoryDto>> SubCategoryListByCategoryId(int categoryId)
        {
            var subCategoryList = await _context.SubCategory.Where(sc => sc.CategoryId == categoryId).ToListAsync();

            return _mapper.Map<List<SubCategoryDto>>(subCategoryList);
        }
    }
}

﻿using AutoMapper;

using Infrastructure.Common;
using Infrastructure.IServices.IHelperService;
using Infrastructure.IServices.ISettingService;
using Infrastructure.Security;

using Microsoft.EntityFrameworkCore;

using Models.Settings;
using Models.Settings.DTO;

using Persistance;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Services.SettingService
{
    public class CategoryService : ICategoryService
    {
        private readonly POSContext _context;
        private readonly IDateFormat _dateFormat;
        private readonly IUserAccessor _userAccessor;
        private readonly IMapper _mapper;

        public CategoryService(POSContext context, IDateFormat dateFormat, IUserAccessor userAccessor, IMapper mapper)
        {
            _context = context;
            _dateFormat = dateFormat;
            _userAccessor = userAccessor;
            _mapper = mapper;
        }

        public Task<CategoryDto> CategoryById(int categoryId)
        {
            throw new NotImplementedException();
        }

        public async Task<List<CategoryDto>> CategoryList()
        {
            var categoryList = await _context.Category
                .Join(
                _context.Brand,
                category => category.Id,
                brand => brand.Id,
                (category,brand) => new CategoryDto
                {
                    Id = category.Id,
                    CategoryName = category.CategoryName,
                    BrandId= brand.Id,
                    BrandName= brand.Name
                })               
                .ToListAsync();
            return _mapper.Map<List<CategoryDto>>(categoryList);
        }

        public async Task<bool> CreateCategory(CategoryDto categoryDto)
        {
            try
            {
                if (categoryDto.Id == 0)
                {
                    var categoryNameExist = await _context.Category.AnyAsync(c => c.CategoryName == categoryDto.CategoryName);

                    if (!categoryNameExist)
                    {
                        var category = new Category
                        {
                            CategoryName = categoryDto.CategoryName,
                            BrandId = categoryDto.BrandId,
                            InsertDate = _dateFormat.Now,
                            PostedBy = _userAccessor.Id
                        };


                        _context.Category.Add(category);
                    }
                    else
                    {
                        throw new RestException(System.Net.HttpStatusCode.Found);
                    }
                }
                else
                {
                    categoryDto.UpdateDate = _dateFormat.Now;
                    categoryDto.ModifiedBy = _userAccessor.UserName;

                    _context.Entry(_mapper.Map<Category>(categoryDto)).State = EntityState.Modified;
                }

                var result = await _context.SaveChangesAsync() > 0;
                return result;
            }
            catch (Exception e)
            {

                throw new RestException(System.Net.HttpStatusCode.BadRequest);
            }

        }

        public Task<bool> DeleteCategory(int categoryId)
        {
            throw new NotImplementedException();
        }
    }
}

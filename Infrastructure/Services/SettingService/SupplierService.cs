﻿using AutoMapper;

using Infrastructure.Common;
using Infrastructure.IServices.IHelperService;
using Infrastructure.IServices.ISettingService;
using Infrastructure.Security;

using Microsoft.EntityFrameworkCore;

using Models.Settings;
using Models.Settings.DTO;

using Persistance;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Services.SettingService
{
    public class SupplierService : ISupplierService
    {
        private readonly POSContext _context;
        private readonly IUserAccessor _userAccessor;
        private readonly IDateFormat _dateFormat;
        private readonly IMapper _mapper;

        public SupplierService(POSContext context, IUserAccessor userAccessor, IDateFormat dateFormat, IMapper mapper)
        {
            this._context = context;
            this._userAccessor = userAccessor;
            this._dateFormat = dateFormat;
            this._mapper = mapper;
        }

        public async Task<bool> CreateSupplier(SupplierDto supplierDto)
        {
            try 
            { 
                if(supplierDto.Id == 0)
                {
                    var supplierExists = await _context.Supplier.AnyAsync(s => s.Name == supplierDto.Name);
                    if (!supplierExists)
                    {
                        Supplier supplier = new Supplier
                        {
                            Name = supplierDto.Name,
                            Address = supplierDto.Address,
                            PhoneNo = supplierDto.PhoneNo,
                            Email = supplierDto.Email,
                            InsertDate = DateTime.Now,

                        };
                        _context.Supplier.Add(supplier);
                     }
                    else
                    {
                        throw new RestException(System.Net.HttpStatusCode.Found, "Exist");
                    }
                }
                else
                {
                    supplierDto.UpdateDate = _dateFormat.Now;

                    _context.Entry(_mapper.Map<Supplier>(supplierDto)).State = EntityState.Modified;
                }

                var result = await _context.SaveChangesAsync() > 0;

                return result;
            }
            catch(Exception ex)
            {
                throw new RestException(System.Net.HttpStatusCode.NotFound, ex.Message);
            }
        }

        public Task<bool> DeleteSupplier(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<SupplierDto>> SupplierList()
        {
            var suppliers = await _context.Supplier.ToListAsync();

            return _mapper.Map<List<SupplierDto>>(suppliers);
        }
    }
}

﻿
using Models.Settings;

using Persistance;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Persistence
{
    public class Seed
    {
        public static async Task SeedData(POSContext context )
        {
            if (!context.MeasurementUnit.Any())
            {
                var units = new List<MeasurementUnit>
                {
                    new MeasurementUnit{ Name = "inch"},
                    new MeasurementUnit{ Name = "feet"},
                    new MeasurementUnit{ Name = "gauge"},
                    new MeasurementUnit{ Name = "meter"},
                    new MeasurementUnit{ Name = "kg"},

                };

                //foreach (var user in units)
                {
                    await context.MeasurementUnit.AddRangeAsync(units);
                }
                await context.SaveChangesAsync();
            }

        }
    }
}

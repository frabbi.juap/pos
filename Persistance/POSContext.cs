﻿using Microsoft.EntityFrameworkCore;

using Models;
using Models.Auth;
using Models.Master;
using Models.Settings;

using System;

namespace Persistance
{
    public class POSContext : DbContext
    {
      public POSContext(DbContextOptions options) : base(options)
        { }

        // authentication , authorization
        public DbSet<Register> Register{ get; set; }
        public DbSet<Login> Login{ get; set; }

        public DbSet<Role> Roles{ get; set; }

        // settings
        public DbSet<Brand> Brand{ get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<SubCategory> SubCategory { get; set; }

        public DbSet<MeasurementUnit> MeasurementUnit { get; set; }
        public DbSet<Supplier> Supplier{ get; set; }
        public DbSet<BusinessType> BusinessTypes{ get; set; }



        // master 

        public DbSet<ProductMasterEntry> ProductMaster { get; set; }

        public DbSet<ProductDetailEntry> ProductDetail { get; set; }
        
        public DbSet<ProductSaleMaster> SaleMaster { get; set; }
        public DbSet<ProductSaleDetail> SaleDetail { get; set; }
    }
}

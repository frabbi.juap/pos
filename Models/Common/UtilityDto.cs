﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Common
{
   public class UtilityDto
    {
        public int Id { get; set; }
        public string PostedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}

﻿using Models.Common;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Settings
{
   public class Supplier : UtilityBase
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string  PhoneNo { get; set; }
        public string Email { get; set; }
    }
}

﻿using Models.Common;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Settings.DTO
{
    public class CategoryDto : UtilityDto
    {
        [Required]
        public string CategoryName { get; set; }

        [Required]
        public int BrandId { get; set; }
        public string BrandName { get; set; }
 
    }

    public class SubCategoryDto : UtilityDto
    {
        [Required]
        public string SubCategoryName { get; set; }

        [Required]
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        //[Required]
        //public int UnitId { get; set; }
        //public string UnitName { get; set; }

        public int BrandId { get; set; }
        public string BrandName { get; set; }        
    }
}

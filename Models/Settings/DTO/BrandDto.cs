﻿using Models.Common;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Settings.DTO
{
   public class BrandDto: UtilityDto
    {
        [Required]
        public string Name { get; set; }

    }
}

﻿using Models.Common;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Settings
{
    public class Category : UtilityBase
    {
        [Required]
        public string CategoryName { get; set; }

        [Required]
        public int BrandId { get; set; }

        //[ForeignKey("BrandId")]
        //public Brand Brand { get; set; }
        public List<SubCategory> SubCategoryList { get; set; }

    }


    public  class SubCategory : UtilityBase
    {
        public string SubCategoryName { get; set; }
        public int CategoryId { get; set; }
       
    }
}

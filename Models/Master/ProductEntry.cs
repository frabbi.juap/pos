﻿using Models.Common;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Master
{
   public class ProductMasterEntry :UtilityBase
    {
        public int CategoryId { get; set; }

        public List<ProductDetailEntry> ProductDetails { get; set; }
    }


    public class ProductDetailEntry : UtilityBase
    {
        public int ProductMasterId { get; set; }
        public int SupplierId { get; set; }
        public int SubCategoryId { get; set; }
        public string Name { get; set; }
        public double Amount { get; set; }
        public double Price { get; set; }
      
    }
}

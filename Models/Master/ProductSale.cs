﻿using Models.Common;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Master
{
    public class ProductSaleMaster: UtilityBase
    {
        public string Voucher { get; set; }
        [NotMapped]
        public List<ProductSaleDetail> SaleProducts { get; set; }
    }
    public class ProductSaleDetail : UtilityBase
    {
        public int VoucherId { get; set; }
        public int ProductId { get; set; }       
        public double SaleQuantity { get; set; }
        public double SalePrice { get; set; }
    }
}

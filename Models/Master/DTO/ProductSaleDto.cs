﻿using Models.Common;

using System.Collections.Generic;

namespace Models.Master.DTO
{
    public class ProductSaleMasterDto : UtilityDto
    {
        public int ProductId { get; set; }
        public string Voucher { get; set; }

        public List<ProductSaleDetailDto> SaleProducts { get; set; }
    }

    public class ProductSaleDetailDto : UtilityDto
    {
        public int VoucherId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public double SaleQuantity { get; set; }
        public double SalePrice { get; set; }
        public double TotalPrice { get; set; }
    }
}

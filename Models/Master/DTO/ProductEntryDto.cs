﻿using Models.Common;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Master.DTO
{
    public class ProductMasterEntryDto : UtilityDto
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public List<ProductDetailEntryDto> ProductDetails { get; set; }
    }


    public class ProductDetailEntryDto : UtilityDto
    {
        public int ProductMasterId { get; set; }
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }

        public int SubCategoryId { get; set; }
        public string SubCategoryName { get; set; }

        public string Name { get; set; }
        public double Amount { get; set; } // quantity
        public double Price { get; set; }

    }
}
